let app = require('express')();
let server = require('http').Server(app);

var mosca = require('mosca');

let bodyParser = require('body-parser');
let moment = require('moment');
let formidable = require("formidable");
let util = require('util');
var firebase = require('firebase-admin');
const request = require('request');

var serviceAccount = require('./dfsmartiot.json');

firebase.initializeApp({
	credential: firebase.credential.cert(serviceAccount),
	databaseURL: 'https://dfsmartiot.firebaseio.com'
});

const DialogflowApp = require('actions-on-google').DialogflowApp;

var devices = {};

var db = firebase.database();

app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.get('', function (req, res) {
	res.render("pages/index",{devices:devices,moment:moment});
});

app.post('/callback', function (request, response) {
	console.log(request.body);
let action = request.body.result.action; 
  let parameters = request.body.result.parameters; 
  let inputContexts = request.body.result.contexts; 
  let requestSource = (request.body.originalRequest) ? request.body.originalRequest.source : undefined;
  const googleAssistantRequest = 'google';

  function sendGoogleResponse (responseToUser) {
    if (typeof responseToUser === 'string') {
      app.ask(responseToUser); // Google Assistant response
    } else {
      // If speech or displayText is defined use it to respond
      let googleResponse = app.buildRichResponse().addSimpleResponse({
        speech: responseToUser.speech || responseToUser.displayText,
        displayText: responseToUser.displayText || responseToUser.speech
      });
      // Optional: Overwrite previous response with rich response
      if (responseToUser.googleRichResponse) {
        googleResponse = responseToUser.googleRichResponse;
      }
      // Optional: add contexts (https://dialogflow.com/docs/contexts)
      if (responseToUser.googleOutputContexts) {
        app.setContext(...responseToUser.googleOutputContexts);
      }
      console.log('Response to Dialogflow (AoG): ' + JSON.stringify(googleResponse));
      app.ask(googleResponse); // Send response to Dialogflow and Google Assistant
    }
  };
    function sendResponse (responseToUser) {
    // if the response is a string send it as a response to the user
    if (typeof responseToUser === 'string') {
      let responseJson = {};
      responseJson.speech = responseToUser; // spoken response
      responseJson.displayText = responseToUser; // displayed response

      response.setHeader('Content-Type', 'application/json');
      response.send(JSON.stringify(responseJson));
      // response.json(responseJson); // Send response to Dialogflow
    } else {
      // If the response to the user includes rich responses or contexts send them to Dialogflow
      let responseJson = {};
      // If speech or displayText is defined, use it to respond (if one isn't defined use the other's value)
      responseJson.speech = responseToUser.speech || responseToUser.displayText;
      responseJson.displayText = responseToUser.displayText || responseToUser.speech;
      // Optional: add rich messages for integrations (https://dialogflow.com/docs/rich-messages)
      responseJson.data = responseToUser.data;
      // Optional: add contexts (https://dialogflow.com/docs/contexts)
      responseJson.contextOut = responseToUser.outputContexts;
      console.log('Response to Dialogflow: ' + JSON.stringify(responseJson));
      response.setHeader('Content-Type', 'application/json');
      response.send(JSON.stringify(responseJson));
    }
  };


  
    const app = new DialogflowApp({request: request, response: response});

      const actionHandlers = {
      'input.state' : () => {
      	const fl = app.getArgument('floor');
      	const room = app.getArgument('room');


              db.ref("smart/office/fl/"+fl+"/"+room+"/state").once("value", function(snapshot) {
              	if (snapshot.val() != null) {

			if (snapshot.val() == 0) {
                sendResponse("The restroom are available");
			}
			else {
				sendResponse("The restroom are not available");
			}
		}

			return null;
      // return event.data.ref.parent.child('uppercase').set(uppercase);
    });
      },
      'input.temperature' : () => {
      	const fl = app.getArgument('floor');
      	const room = app.getArgument('room');

              db.ref("smart/office/fl/"+fl+"/"+room+"/temperature").once("value", function(snapshot) {
              	if (snapshot.val() != null) {

                sendResponse("The temperature in "+room+" floor "+fl+" is " + snapshot.val()+" C.");
			
		}

			return null;
      // return event.data.ref.parent.child('uppercase').set(uppercase);
    });
      },
    // Default handler for unknown or undefined actions
    'default': () => {
      // Use the Actions on Google lib to respond to Google requests; for other requests use JSON
      if (requestSource === googleAssistantRequest) {
        let responseToUser = {
          //googleRichResponse: googleRichResponse, // Optional, uncomment to enable
          //googleOutputContexts: ['weather', 2, { ['city']: 'rome' }], // Optional, uncomment to enable
          speech: 'This message is from Dialogflow\'s Cloud Functions for Firebase editor!', // spoken response
          text: 'This is from Dialogflow\'s Cloud Functions for Firebase editor! :-)' // displayed response
        };
        sendGoogleResponse(responseToUser);
      } else {
        let responseToUser = {
          //data: richResponsesV1, // Optional, uncomment to enable
          //outputContexts: [{'name': 'weather', 'lifespan': 2, 'parameters': {'city': 'Rome'}}], // Optional, uncomment to enable
          speech: 'This message is from Dialogflow\'s Cloud Functions for Firebase editor!', // spoken response
          text: 'This is from Dialogflow\'s Cloud Functions for Firebase editor! :-)' // displayed response
        };
        sendResponse(responseToUser);
      }
    }};


    // app.handleRequest(handlerRequest);

      if (!actionHandlers[action]) {
    action = 'default';
  }
  // Run the proper handler function to handle the request from Dialogflow
  actionHandlers[action]();

});


// db.ref('/devices/{devicesId}/status/{type}').on("value",event => {

//     	    db.ref("/devices/"+event.params.devicesId+"/maps/"+event.params.type).once("value", function(snapshot) {
// 			if (snapshot.val() != null) {
// 				update = {};
// 				update[snapshot.val()] = event.data.val();
// 				db.ref().update(update);
// 			}
//     });
//     return null;


// });


var settings = {
	port: 1883
};

var serverMosca = new mosca.Server(settings);

serverMosca.on('ready', setup);

function createDevice(id) {
	if (id) {
		db.ref("/devices/"+id).set({create_at:Date.now(),state:0,pins:{input:0,output:0},status:0});
	}
}

function updateWeather() {
	console.log("update weather");
	request('http://api.openweathermap.org/data/2.5/weather?id=1609350&appid=8726701f03d5c66b9b39563ff75835d1&units=metric&t='+Date.now(), { json: true }, (err, res, body) => {
		if (err) { return console.log(err); }
  // console.log(body);
  db.ref().update({weather:body});});
}

setInterval(updateWeather, 10000);

// Accepts the connection if the username and password are valid
var authenticate = function(client, username, password, callback) {
	// callback(null, true);

	// if (username === "admin" && password == "P@ssw0rd") {
	// 	callback(null, true);
	// }
	// else {

		db.ref("/devices/"+client.id).once("value", function(snapshot) {
			if (snapshot.val()) {
				callback(null, true);
			}
			else{
				createDevice(client.id);
				callback(null, true);
			}
		});
	// }
}

// In this case the client authorized as alice can publish to /users/alice taking
// the username from the topic and verifing it is the same of the authorized user
var authorizePublish = function(client, topic, payload, callback) {
	// console.log(client);
	callback(null, true);
	// if (username === "admin" && password == "P@ssw0rd") {
	// 	callback(null, true);
	// }
	// else {

	// 	db.ref("/devices/"+client.id).once("value", function(snapshot) {
	// 		if (snapshot.val()) {
	// 			callback(null, true);
	// 		}
	// 		else{
	// 			callback(null, false);
	// 		}
	// 	});
	// }
}

// In this case the client authorized as alice can subscribe to /users/alice taking
// the username from the topic and verifing it is the same of the authorized user
var authorizeSubscribe = function(client, topic, callback) {
	if (client) {

		db.ref("/devices/"+client.id).once("value", function(snapshot) {
			if (snapshot.val()) {
				callback(null, true);
				
				if (topic.endsWith("/config")) {
					db.ref("/devices/"+client.id+"config").once("value", function(snapshot) {
						if (snapshot.val()) {
							var packet = {
								topic: client.id+'/config',
								payload: snapshot.val(),
								qos: 1,
								retain: false,  
							};
							serverMosca.publish(packet);
						}
					});
				}


			}
			else{
				callback(null, false);
			}
		});
	}
	
}


// fired when a message is published
serverMosca.on('published', function(packet, client) {
	if (client) {
		db.ref("/devices/"+client.id).once("value", function(snapshot) {
			if (snapshot.val()) {
				db.ref("/devices/"+client.id).update({last_connect:Date.now()});

				if (packet.topic.endsWith("/state")) {
					var data = JSON.parse(packet.payload);
					db.ref("/devices/"+client.id).update({state:data});
				}
				else if (packet.topic.endsWith("/updateInterval")) {
					var data = JSON.parse(packet.payload);
					data.update_at = Date.now();
					db.ref("/devices/"+client.id).update({status:data});
				}
				else if (packet.topic.endsWith("/updatestate")) {
					var data = JSON.parse(packet.payload);
					var update = {};
					update[data.pin] = data.state;
					db.ref("/devices/"+client.id+"/pins/input").update(update);

					   db.ref("/devices/"+client.id+"/maps/input"+data.pin).once("value", function(snapshot) {
			if (snapshot.val() != null) {
				updatem = {};
				updatem[snapshot.val()] = data.state;
				db.ref().update(updatem);
			}
    });
				}
				// else {
					// console.log(packet);
				// }
			}
			
		});
	}
});

// fired when a client connects
serverMosca.on('clientConnected', function(client) {
	devices[client.id] = {connect_at:new Date(),ip:client.connection.stream.remoteAddress};
	console.log('Client Connected:', client.id);

	db.ref("/devices/"+client.id).update({online:true});

	db.ref("/devices/"+client.id+"/maps/online").once("value", function(snapshot) {
	if (snapshot.val() != null) {
		update = {};
		update[snapshot.val()] = true;
		db.ref().update(update);
	}
	});
});

// fired when a client disconnects
serverMosca.on('clientDisconnected', function(client) {
	delete devices[client.id];
	console.log('Client Disconnected:', client.id);
	
	db.ref("/devices/"+client.id).update({online:false});

	db.ref("/devices/"+client.id+"/maps/online").once("value", function(snapshot) {
	if (snapshot.val() != null) {
		update = {};
		update[snapshot.val()] = false;
		db.ref().update(update);
	}
	});

});

// fired when the mqtt server is ready
function setup() {
	serverMosca.authenticate = authenticate;
	// serverMosca.authorizePublish = authorizePublish;
	// serverMosca.authorizeSubscribe = authorizeSubscribe;
	console.log('Mosca server is up and running')
}


// var ref = db.ref("/");
// ref.once("value", function(snapshot) {
//   console.log(snapshot.val());
// });

server.listen(8000);

process.stdin.resume();//so the program will not close instantly

function exitHandler(options, err) {
	Object.keys(devices).forEach(function(key) {
		console.log(key);
  db.ref("/devices/"+key).update({online:false});
});
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
    setTimeout(function() {
    	if (options.exit) process.exit();
    }, 3000);
    
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));